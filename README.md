## Necessary Packages
-Nodejs
-Npm

### HOW REACT WORKS

public/index.html is the page template;

src/index.js is the JavaScript entry point.

So, react will start in index.html, and in index.js the react get the component App (project component container) and all childrens and put inside div #root

### HOW RUN:

## 1- EXECUTE `npm install` (it will download all libs necessary)
## 2- EXECUTE `npm start` (to start the project)

-Runs the app in the development mode.
-Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits
You will also see any lint errors in the console.